@extends('layouts.app')

@section('botones')
    <a href="{{ route('recetas.index') }}" class="btn btn-outline-primary mr-2 text-upercase font-weigth-bold">
        <svg class="icono" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 15l-3-3m0 0l3-3m-3 3h8M3 12a9 9 0 1118 0 9 9 0 01-18 0z"></path></svg>
        volver
    </a>
@endsection

@section('content')
    <div class="col-md-10 mx-auto bg-white p-3">
        {{-- <h1>{{ $receta }}</h1> --}}

        <article class="contenido-receta">
            <h1 class="text-center mb-4"> {{ __($receta->titulo)}}</h1>

            <div class="imagen-receta">
                <img src="/storage/{{$receta->imagen}}" alt="receta imagen" height="200" width="600" class="mx-auto d-block">
            </div>

            <div class="receta-meta">
                <p>
                    <span class="font-weight-bold text-primary"> Esctrito en:</span>
                    {{$receta->categoria->nombre}}
                </p>

                <p>
                    <span class="font-weight-bold text-primary"> Autor:</span>
                    {{-- TODO: mostrar el usuario --}}
                    {{$receta->autor->name}}
                </p>

                <p>
                    <span class="font-weight-bold text-primary"> Fecha:</span>
                    {{-- TODO: mostrar el usuario --}}
                    
                    @php
                        $fecha = $receta->created_at;
                    @endphp
                    <fecha-receta fecha='{{$fecha}}'></fecha-receta>
                </p>

                <div class="ingredientes">
                    <h2 class="my-3 text-primary">Ingredientes:</h2>
                    {!!$receta->ingredientes!!}
                </div>

                <div class="preparacion">
                    <h2 class="my-3 text-primary">Preparación:</h2>
                    {!!$receta->preparacion!!}
                </div>

                <like-button
                    receta-id="{{$receta->id}}">
                </like-button>

            </div>
        </article>
    </div>

    
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>    
@endsection