<?php

namespace App\Http\Controllers;

use App\Models\Perfil;
use App\Models\Receta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class PerfilController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'show']);
    }

    public function show(Perfil $perfil)
    {
        //
        $recetas = Receta::where('user_id', $perfil->user_id)->paginate(9);
        return view('perfiles.show', compact('perfil', 'recetas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Perfil  $perfil
     * @return \Illuminate\Http\Response
     */
    public function edit(Perfil $perfil)
    {
        //verificar que el usuario logueado sea el que pueda modificar el perfil
        Gate::authorize('view', $perfil);

        //obtener las recetas con paginacion 
        $recetas = Receta::where('user_id', $perfil->user_id)->paginate(2);
        //
        return view('perfiles.edit', compact('perfil', 'recetas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Perfil  $perfil
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Perfil $perfil)
    {
        //verificar que el usuario logueado sea el que pueda modificar el perfil
        Gate::authorize('update', $perfil);

        //validar
        $data = request()->validate([
            'nombre' => 'required',
            'url' => 'required',
            'biografia' => 'required',
        ]);

        //si el usuario sube una imagen
        if ($request['imagen']) {
            //obtener la ruta de la imagen
            $ruta_imagen = $request['imagen']->store('upload-perfiles', 'public');

            //Resize de la imagen
            //$img = Image::make( public_path("storage/{$ruta_imagen}"))->fit(1000,550);
            //$img->save();

            //crear un arreglo de imagen
            $array_imagen = ['imagen' => $ruta_imagen];
        }

        //asignar nombre y url
        auth()->user()->url = $data['url'];
        auth()->user()->name = $data['nombre'];
        auth()->user()->save();

        //eliminar url y name de $data
        unset($data['url']);
        unset($data['nombre']);


        //asignar biografiá e imagen
        auth()->user()->perfil()->update(array_merge(
            $data, //actualiza unicamente el campo biografia de perfiles
            $array_imagen ?? []
        ));


        //guardar

        //redireccionar
        return redirect()->action([RecetaController::class, 'index']);
    }
}
