<?php

namespace App\Http\Controllers;

use App\Models\Receta;
use Illuminate\Http\Request;
use App\Models\CategoriaReceta;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use League\CommonMark\Extension\CommonMark\Node\Inline\Image;

class RecetaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'show']);
    }

    public function index()
    {
        //$recetas = Auth::user()->recetas;
        // dd($recetas);

        $usuario = auth()->user()->id;
        //recetas con paginacion
        $recetas = Receta::where('user_id', $usuario)->paginate(3);

        return view('recetas.index')->with('recetas', $recetas);
    }

    public function create()
    {
        //DB::table('categoria_receta')->get()->dd();

        //obtener categorias sin modelo
        //$categorias = DB::table('categoria_recetas')->get()->pluck('nombre', 'id');

        //obtener con modelo 
        $categorias = CategoriaReceta::all(['id', 'nombre']);
        return view('recetas.create')->with('categorias', $categorias);
    }

    public function store(Request $request)
    {
        //validacion
        $data = $request->validate([
            'titulo' => 'required|min:6',
            'preparacion' => 'required',
            'ingredientes' => 'required',
            'imagen' => 'required|image',
            'categoria' => 'required'
        ]);

        //obtener ruta de la imagen
        $ruta_imagen = $request['imagen']->store('upload-recetas', 'public');

        //resize de la imagen
        //$img = Image::make(public_path("storage/{$ruta_imagen}"))->fit(1000, 550);

        //Resize de la imagen
        // $img = Image::make(public_path("storage/{$ruta_imagen}"))->fit(1000, 550);
        // $img->save();

        //almacenar en la base de datos (sin modelo)
        // DB::table('recetas')->insert([
        //     'titulo' => $data['titulo'],
        //     'preparacion' => $data['preparacion'],
        //     'ingredientes' => $data['ingredientes'],
        //     'imagen' => $ruta_imagen,
        //     'user_id' => Auth::user()->id,
        //     'categoria_id' => $data['categoria']
        // ]);

        //Almacenar en la base de datos (con modelo)
        auth()->user()->recetas()->create([
            'titulo' => $data['titulo'],
            'preparacion' => $data['preparacion'],
            'ingredientes' => $data['ingredientes'],
            'imagen' => $ruta_imagen,
            'categoria_id' => $data['categoria'],
        ]);

        return redirect()->action([RecetaController::class, 'index']); // redirecciona luego de agregar al formulario 

        // dd($request->all()); //para ver los inputs pasados 
    }

    public function show(Receta $receta)
    {
        return view('recetas.show')->with('receta', $receta);
    }

    public function edit(Receta $receta)
    {
        Gate::authorize('view', $receta);

        $categorias = CategoriaReceta::all(['id', 'nombre']);
        return view('recetas.edit', compact('categorias', 'receta'));
    }

    public function update(Request $request,  Receta $receta)
    {

        Gate::authorize('update', $receta);

        //validacion
        $data = $request->validate([
            'titulo' => 'required|min:6',
            'preparacion' => 'required',
            'ingredientes' => 'required',
            'categoria' => 'required'
        ]);

        //asignar los valores
        $receta->titulo = $data['titulo'];
        $receta->preparacion = $data['preparacion'];
        $receta->ingredientes = $data['ingredientes'];
        $receta->categoria_id = $data['categoria'];

        //si el usuario sube una nueva imagen 
        if (request('imagen')) {
            //obtener la ruta de la imagen
            $ruta_imagen = $request['imagen']->store('upload-recetas', 'public');

            //resize de la imagen
            //$img = Image::make(public_path("storage/{$ruta_imagen}"))->fit(1000, 550);
            //$img->save();

            //asignamos al objeto
            $receta->imagen = $ruta_imagen;
            $receta->save();
        }

        $receta->save();

        //redireccionar 
        return redirect()->action([RecetaController::class, 'index']);
    }

    public function destroy(Receta $receta)
    {
        //ejecutar el policy 
        Gate::authorize('update', $receta);

        //eliminar receta
        $receta->delete();

        return redirect()->action([RecetaController::class, 'index']);
    }
}
