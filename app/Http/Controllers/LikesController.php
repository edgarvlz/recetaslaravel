<?php

namespace App\Http\Controllers;

use App\Models\Perfil;
use App\Models\Receta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class LikesController extends Controller
{
    //almacena los likes de un usuario a una receta
    public function update(Request $request, Receta $receta)
    {
        //
        return auth()->user()->meGusta()->toggle($receta);
    }
}
