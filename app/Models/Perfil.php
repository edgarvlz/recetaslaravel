<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Perfil extends Model
{
    use HasFactory;

    protected $table = 'public.perfiles';
    protected $primaryKey = 'id';

    //relacion de 1:1 de Perfil con usuario
    public function usuario()
    {
        return $this->belongsTo(user::class, 'user_id');
    }
}
