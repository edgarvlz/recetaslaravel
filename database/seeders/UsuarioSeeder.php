<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Edgar Valen',
            'email' => 'edgarjaed@gmail.com',
            'password' => Hash::make('12345678'),
            'url' => 'http://codigoedgar.com',
        ]);
        //$user->perfil()->create();

        $user = User::create([
            'name' => 'Edgar Valen',
            'email' => 'andriuws14@hotmail.com',
            'password' => Hash::make('12345678'),
            'url' => 'http://codigoedgar.com',
        ]);
        //$user->perfil()->create();
    }
}
